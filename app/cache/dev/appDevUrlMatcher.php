<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/css/general')) {
            // _assetic_edd775c
            if ($pathinfo === '/css/general.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'edd775c',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_edd775c',);
            }

            if (0 === strpos($pathinfo, '/css/general_')) {
                // _assetic_edd775c_0
                if ($pathinfo === '/css/general_reset_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'edd775c',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_edd775c_0',);
                }

                if (0 === strpos($pathinfo, '/css/general_bootstrap')) {
                    // _assetic_edd775c_1
                    if ($pathinfo === '/css/general_bootstrap.min_2.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'edd775c',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_edd775c_1',);
                    }

                    // _assetic_edd775c_2
                    if ($pathinfo === '/css/general_bootstrap-theme.min_3.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'edd775c',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_edd775c_2',);
                    }

                }

                // _assetic_edd775c_3
                if ($pathinfo === '/css/general_part_4_main_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'edd775c',  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_edd775c_3',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/b5e3653')) {
            // _assetic_b5e3653
            if ($pathinfo === '/js/b5e3653.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'b5e3653',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_b5e3653',);
            }

            if (0 === strpos($pathinfo, '/js/b5e3653_')) {
                // _assetic_b5e3653_0
                if ($pathinfo === '/js/b5e3653_jquery.min_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'b5e3653',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_b5e3653_0',);
                }

                // _assetic_b5e3653_1
                if ($pathinfo === '/js/b5e3653_bootstrap.min_2.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'b5e3653',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_b5e3653_1',);
                }

                // _assetic_b5e3653_2
                if ($pathinfo === '/js/b5e3653_main_3.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'b5e3653',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_b5e3653_2',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // films
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'films');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\FilmController::indexAction',  '_route' => 'films',);
        }

        if (0 === strpos($pathinfo, '/film')) {
            // new_film
            if ($pathinfo === '/film/new') {
                return array (  '_controller' => 'AppBundle\\Controller\\FilmController::newAction',  '_route' => 'new_film',);
            }

            // create_film
            if ($pathinfo === '/film/create') {
                return array (  '_controller' => 'AppBundle\\Controller\\FilmController::createAction',  '_route' => 'create_film',);
            }

        }

        // films_dashboard
        if ($pathinfo === '/dashboard/films') {
            return array (  '_controller' => 'AppBundle\\Controller\\FilmController::dashboardFilmsAction',  '_route' => 'films_dashboard',);
        }

        if (0 === strpos($pathinfo, '/film')) {
            // edit_film
            if (0 === strpos($pathinfo, '/film/edit') && preg_match('#^/film/edit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_film')), array (  '_controller' => 'AppBundle\\Controller\\FilmController::editAction',));
            }

            // update_film
            if (0 === strpos($pathinfo, '/film/update') && preg_match('#^/film/update/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'update_film')), array (  '_controller' => 'AppBundle\\Controller\\FilmController::updateAction',));
            }

            // delete_film
            if (0 === strpos($pathinfo, '/film/delete') && preg_match('#^/film/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_film')), array (  '_controller' => 'AppBundle\\Controller\\FilmController::deleteAction',));
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
