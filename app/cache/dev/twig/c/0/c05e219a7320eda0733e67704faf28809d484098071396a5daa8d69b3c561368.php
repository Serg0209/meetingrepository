<?php

/* AppBundle::dashboard.html.twig */
class __TwigTemplate_73838b6b258d7e67b13d8fda1ba65f29fe70d00b44e8d67a223131b5cb1a87a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle::dashboard.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd928ff4647d3b94a80347ef187861de847100a498c06b25cb3ce5080a12de4b = $this->env->getExtension("native_profiler");
        $__internal_dd928ff4647d3b94a80347ef187861de847100a498c06b25cb3ce5080a12de4b->enter($__internal_dd928ff4647d3b94a80347ef187861de847100a498c06b25cb3ce5080a12de4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::dashboard.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dd928ff4647d3b94a80347ef187861de847100a498c06b25cb3ce5080a12de4b->leave($__internal_dd928ff4647d3b94a80347ef187861de847100a498c06b25cb3ce5080a12de4b_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_94ff2c97d2648f862838f42c9960f1112d10fb6b4dda47a0964cbfc735f87cd3 = $this->env->getExtension("native_profiler");
        $__internal_94ff2c97d2648f862838f42c9960f1112d10fb6b4dda47a0964cbfc735f87cd3->enter($__internal_94ff2c97d2648f862838f42c9960f1112d10fb6b4dda47a0964cbfc735f87cd3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <br>
    <a class=\"btn btn-success\" href=\"";
        // line 4
        echo $this->env->getExtension('routing')->getPath("new_film");
        echo "\">Добавить фильм</a>
    <h3>Список фильмов</h3>
    ";
        // line 6
        if ((twig_length_filter($this->env, (isset($context["films"]) ? $context["films"] : $this->getContext($context, "films"))) > 0)) {
            // line 7
            echo "    <table class=\"table table-bordered table-striped\">
        <thead>
            <tr>
                <th>ID</th>
                <th>Название фильма</th>
                <th>Год выпуска</th>
                <th>Опубликованый?</th>
                <th>Действия</th>
            </tr>
        </thead>

            <tbody class=\"table-striped\">
            ";
            // line 19
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["films"]) ? $context["films"] : $this->getContext($context, "films")));
            foreach ($context['_seq'] as $context["_key"] => $context["film"]) {
                // line 20
                echo "                <tr>
                    <td>";
                // line 21
                echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "id", array()), "html", null, true);
                echo "</td>
                    <td>";
                // line 22
                echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "name", array()), "html", null, true);
                echo "</td>
                    <td>";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "year", array()), "html", null, true);
                echo "</td>
                    <td>
                        ";
                // line 25
                if (($this->getAttribute($context["film"], "isActive", array()) == true)) {
                    // line 26
                    echo "                            <span class=\"label label-success\">Да</span>
                        ";
                } else {
                    // line 28
                    echo "                            <span class=\"label label-danger\">Нет</span>
                        ";
                }
                // line 30
                echo "                    </td>
                    <td>
                        <a class=\"label label-primary\" href=\"";
                // line 32
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("edit_film", array("id" => $this->getAttribute($context["film"], "id", array()))), "html", null, true);
                echo "\">Edit</a>
                        <a class=\"label label-danger\" href=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("delete_film", array("id" => $this->getAttribute($context["film"], "id", array()))), "html", null, true);
                echo "\">Delete</a>
                    </td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['film'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo "            </tbody>
    </table>
    ";
        } else {
            // line 40
            echo "        Сейчас нет активных фильмов
    ";
        }
        
        $__internal_94ff2c97d2648f862838f42c9960f1112d10fb6b4dda47a0964cbfc735f87cd3->leave($__internal_94ff2c97d2648f862838f42c9960f1112d10fb6b4dda47a0964cbfc735f87cd3_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle::dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 40,  112 => 37,  102 => 33,  98 => 32,  94 => 30,  90 => 28,  86 => 26,  84 => 25,  79 => 23,  75 => 22,  71 => 21,  68 => 20,  64 => 19,  50 => 7,  48 => 6,  43 => 4,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block body %}*/
/*     <br>*/
/*     <a class="btn btn-success" href="{{ path('new_film') }}">Добавить фильм</a>*/
/*     <h3>Список фильмов</h3>*/
/*     {% if films|length > 0 %}*/
/*     <table class="table table-bordered table-striped">*/
/*         <thead>*/
/*             <tr>*/
/*                 <th>ID</th>*/
/*                 <th>Название фильма</th>*/
/*                 <th>Год выпуска</th>*/
/*                 <th>Опубликованый?</th>*/
/*                 <th>Действия</th>*/
/*             </tr>*/
/*         </thead>*/
/* */
/*             <tbody class="table-striped">*/
/*             {% for film in films %}*/
/*                 <tr>*/
/*                     <td>{{ film.id }}</td>*/
/*                     <td>{{ film.name }}</td>*/
/*                     <td>{{ film.year }}</td>*/
/*                     <td>*/
/*                         {% if film.isActive == true %}*/
/*                             <span class="label label-success">Да</span>*/
/*                         {% else %}*/
/*                             <span class="label label-danger">Нет</span>*/
/*                         {% endif %}*/
/*                     </td>*/
/*                     <td>*/
/*                         <a class="label label-primary" href="{{ path('edit_film', {'id': film.id}) }}">Edit</a>*/
/*                         <a class="label label-danger" href="{{ path('delete_film', {'id': film.id}) }}">Delete</a>*/
/*                     </td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </tbody>*/
/*     </table>*/
/*     {% else %}*/
/*         Сейчас нет активных фильмов*/
/*     {% endif %}*/
/* {% endblock %}*/
