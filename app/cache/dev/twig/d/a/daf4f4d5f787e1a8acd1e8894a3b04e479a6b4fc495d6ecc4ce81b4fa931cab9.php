<?php

/* ::base.html.twig */
class __TwigTemplate_e8597595b3544685dffa0f4ef5970b14a1a3ce261fa254fc11c3cbccb067be62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f145d801e863eeeb08f7b756057efe4e270326ea1f23534b20ffead6c096e4ad = $this->env->getExtension("native_profiler");
        $__internal_f145d801e863eeeb08f7b756057efe4e270326ea1f23534b20ffead6c096e4ad->enter($__internal_f145d801e863eeeb08f7b756057efe4e270326ea1f23534b20ffead6c096e4ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\" />
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
</head>
<body>
";
        // line 20
        $context["route"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method");
        // line 21
        echo "<ul class=\"nav nav-tabs\">
    <li role=\"presentation\" ";
        // line 22
        if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) == "films")) {
            echo "class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("films");
        echo "\">Фильмы</a></li>
    <li role=\"presentation\" ";
        // line 23
        if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) != "films")) {
            echo "class=\"active\"";
        }
        echo " ><a  href=\"";
        echo $this->env->getExtension('routing')->getPath("films_dashboard");
        echo "\">Панель управления</a></li>
</ul>
<div id=\"wrapper\" class=\"container\">
    ";
        // line 26
        $this->displayBlock('body', $context, $blocks);
        // line 27
        echo "</div>
";
        // line 28
        $this->displayBlock('javascripts', $context, $blocks);
        // line 37
        echo "</body>
</html>";
        
        $__internal_f145d801e863eeeb08f7b756057efe4e270326ea1f23534b20ffead6c096e4ad->leave($__internal_f145d801e863eeeb08f7b756057efe4e270326ea1f23534b20ffead6c096e4ad_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_32f5f6d85b27b7916504f52bb9dadd74ea89dfefe0acf659194d7189d31a521c = $this->env->getExtension("native_profiler");
        $__internal_32f5f6d85b27b7916504f52bb9dadd74ea89dfefe0acf659194d7189d31a521c->enter($__internal_32f5f6d85b27b7916504f52bb9dadd74ea89dfefe0acf659194d7189d31a521c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_32f5f6d85b27b7916504f52bb9dadd74ea89dfefe0acf659194d7189d31a521c->leave($__internal_32f5f6d85b27b7916504f52bb9dadd74ea89dfefe0acf659194d7189d31a521c_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_40abfbef3aec19a5498ebb14bacc565f6e571f0326434902277971af6d13f459 = $this->env->getExtension("native_profiler");
        $__internal_40abfbef3aec19a5498ebb14bacc565f6e571f0326434902277971af6d13f459->enter($__internal_40abfbef3aec19a5498ebb14bacc565f6e571f0326434902277971af6d13f459_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "        ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "edd775c_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_edd775c_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/general_reset_1.css");
            // line 14
            echo "        <link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" type=\"text/css\" rel=\"stylesheet\" />
        ";
            // asset "edd775c_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_edd775c_1") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/general_bootstrap.min_2.css");
            echo "        <link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" type=\"text/css\" rel=\"stylesheet\" />
        ";
            // asset "edd775c_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_edd775c_2") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/general_bootstrap-theme.min_3.css");
            echo "        <link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" type=\"text/css\" rel=\"stylesheet\" />
        ";
            // asset "edd775c_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_edd775c_3") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/general_part_4_main_1.css");
            echo "        <link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" type=\"text/css\" rel=\"stylesheet\" />
        ";
        } else {
            // asset "edd775c"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_edd775c") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/general.css");
            echo "        <link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" type=\"text/css\" rel=\"stylesheet\" />
        ";
        }
        unset($context["asset_url"]);
        // line 16
        echo "    ";
        
        $__internal_40abfbef3aec19a5498ebb14bacc565f6e571f0326434902277971af6d13f459->leave($__internal_40abfbef3aec19a5498ebb14bacc565f6e571f0326434902277971af6d13f459_prof);

    }

    // line 26
    public function block_body($context, array $blocks = array())
    {
        $__internal_097300b4f3e3c13798e8279ab5fb9da53cfbd8deff908ed01514d05dbbc6a3f7 = $this->env->getExtension("native_profiler");
        $__internal_097300b4f3e3c13798e8279ab5fb9da53cfbd8deff908ed01514d05dbbc6a3f7->enter($__internal_097300b4f3e3c13798e8279ab5fb9da53cfbd8deff908ed01514d05dbbc6a3f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_097300b4f3e3c13798e8279ab5fb9da53cfbd8deff908ed01514d05dbbc6a3f7->leave($__internal_097300b4f3e3c13798e8279ab5fb9da53cfbd8deff908ed01514d05dbbc6a3f7_prof);

    }

    // line 28
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_8e25e6edc387ef4d4215b60c59d815c9afc237c9eea0b4e324e15fb48d94a047 = $this->env->getExtension("native_profiler");
        $__internal_8e25e6edc387ef4d4215b60c59d815c9afc237c9eea0b4e324e15fb48d94a047->enter($__internal_8e25e6edc387ef4d4215b60c59d815c9afc237c9eea0b4e324e15fb48d94a047_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 29
        echo "    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "b5e3653_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_b5e3653_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/b5e3653_jquery.min_1.js");
            // line 34
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
            // asset "b5e3653_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_b5e3653_1") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/b5e3653_bootstrap.min_2.js");
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
            // asset "b5e3653_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_b5e3653_2") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/b5e3653_main_3.js");
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "b5e3653"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_b5e3653") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/b5e3653.js");
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
        
        $__internal_8e25e6edc387ef4d4215b60c59d815c9afc237c9eea0b4e324e15fb48d94a047->leave($__internal_8e25e6edc387ef4d4215b60c59d815c9afc237c9eea0b4e324e15fb48d94a047_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 34,  162 => 29,  156 => 28,  145 => 26,  138 => 16,  106 => 14,  101 => 7,  95 => 6,  83 => 5,  75 => 37,  73 => 28,  70 => 27,  68 => 26,  58 => 23,  50 => 22,  47 => 21,  45 => 20,  38 => 17,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/*     <meta charset="UTF-8" />*/
/*     <title>{% block title %}Welcome!{% endblock %}</title>*/
/*     {% block stylesheets %}*/
/*         {% stylesheets  filter='cssrewrite'*/
/*         '@AppBundle/Resources/public/css/reset.css'*/
/*         '@AppBundle/Resources/public/css/bootstrap.min.css'*/
/*         '@AppBundle/Resources/public/css/bootstrap-theme.min.css'*/
/*         '@AppBundle/Resources/public/less/*'*/
/*         output='css/general.css'*/
/*         %}*/
/*         <link href="{{ asset_url }}" type="text/css" rel="stylesheet" />*/
/*         {% endstylesheets %}*/
/*     {% endblock %}*/
/*     <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/* </head>*/
/* <body>*/
/* {% set route = app.request.get('_route') %}*/
/* <ul class="nav nav-tabs">*/
/*     <li role="presentation" {% if route == 'films' %}class="active"{% endif %}><a href="{{ path('films') }}">Фильмы</a></li>*/
/*     <li role="presentation" {% if route != 'films' %}class="active"{% endif %} ><a  href="{{ path('films_dashboard') }}">Панель управления</a></li>*/
/* </ul>*/
/* <div id="wrapper" class="container">*/
/*     {% block body %}{% endblock %}*/
/* </div>*/
/* {% block javascripts %}*/
/*     {% javascripts*/
/*     '@AppBundle/Resources/public/scripts/jquery.min.js'*/
/*     '@AppBundle/Resources/public/scripts/bootstrap.min.js'*/
/*     '@AppBundle/Resources/public/scripts/main.js'*/
/*     %}*/
/*     <script src="{{ asset_url }}"></script>*/
/*     {% endjavascripts %}*/
/* {% endblock %}*/
/* </body>*/
/* </html>*/
