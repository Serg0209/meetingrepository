<?php

/* AppBundle::index.html.twig */
class __TwigTemplate_beb8b45caa62b3bcd0a2ce39a976be8b38529193973356b0b08c9c374d54aa1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle::index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_68ff6c31ed771b4998b29225762bfee1fc8b766b88a8f644511e22dcd093aeba = $this->env->getExtension("native_profiler");
        $__internal_68ff6c31ed771b4998b29225762bfee1fc8b766b88a8f644511e22dcd093aeba->enter($__internal_68ff6c31ed771b4998b29225762bfee1fc8b766b88a8f644511e22dcd093aeba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_68ff6c31ed771b4998b29225762bfee1fc8b766b88a8f644511e22dcd093aeba->leave($__internal_68ff6c31ed771b4998b29225762bfee1fc8b766b88a8f644511e22dcd093aeba_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_8957e1f748eeb7ac45d5de19d21e70fd1d957ba957c393d6cad78a29edf3ccd4 = $this->env->getExtension("native_profiler");
        $__internal_8957e1f748eeb7ac45d5de19d21e70fd1d957ba957c393d6cad78a29edf3ccd4->enter($__internal_8957e1f748eeb7ac45d5de19d21e70fd1d957ba957c393d6cad78a29edf3ccd4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <h3>Список фильмов</h3>
    ";
        // line 4
        if ((twig_length_filter($this->env, (isset($context["films"]) ? $context["films"] : $this->getContext($context, "films"))) > 0)) {
            // line 5
            echo "    <table class=\"table table-bordered table-striped\">
        <thead>
            <tr>
                <th>Название фильма</th>
                <th>Год выпуска</th>
            </tr>
        </thead>
            <tbody class=\"table-striped\">
            ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["films"]) ? $context["films"] : $this->getContext($context, "films")));
            foreach ($context['_seq'] as $context["_key"] => $context["film"]) {
                // line 14
                echo "                <tr>
                    <td>";
                // line 15
                echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "name", array()), "html", null, true);
                echo "</td>
                    <td>";
                // line 16
                echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "year", array()), "html", null, true);
                echo "</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['film'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "            </tbody>
    </table>
    ";
        } else {
            // line 22
            echo "        Сейчас нет активных фильмов
    ";
        }
        
        $__internal_8957e1f748eeb7ac45d5de19d21e70fd1d957ba957c393d6cad78a29edf3ccd4->leave($__internal_8957e1f748eeb7ac45d5de19d21e70fd1d957ba957c393d6cad78a29edf3ccd4_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle::index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 22,  75 => 19,  66 => 16,  62 => 15,  59 => 14,  55 => 13,  45 => 5,  43 => 4,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block body %}*/
/*     <h3>Список фильмов</h3>*/
/*     {% if films|length > 0 %}*/
/*     <table class="table table-bordered table-striped">*/
/*         <thead>*/
/*             <tr>*/
/*                 <th>Название фильма</th>*/
/*                 <th>Год выпуска</th>*/
/*             </tr>*/
/*         </thead>*/
/*             <tbody class="table-striped">*/
/*             {% for film in films %}*/
/*                 <tr>*/
/*                     <td>{{ film.name }}</td>*/
/*                     <td>{{ film.year }}</td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </tbody>*/
/*     </table>*/
/*     {% else %}*/
/*         Сейчас нет активных фильмов*/
/*     {% endif %}*/
/* {% endblock %}*/
