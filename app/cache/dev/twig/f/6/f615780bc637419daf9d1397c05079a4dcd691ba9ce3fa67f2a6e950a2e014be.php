<?php

/* TwigBundle:Exception:traces.txt.twig */
class __TwigTemplate_0e067d7f5abc13c6f0077bf6cf1f5102f046fcfa643fb7dea9b805b826a4ed0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8cc0eef3b32707349c6a7e3f5b2059c8e51ffc75862db16d8a0f0415d473c338 = $this->env->getExtension("native_profiler");
        $__internal_8cc0eef3b32707349c6a7e3f5b2059c8e51ffc75862db16d8a0f0415d473c338->enter($__internal_8cc0eef3b32707349c6a7e3f5b2059c8e51ffc75862db16d8a0f0415d473c338_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("TwigBundle:Exception:trace.txt.twig", "TwigBundle:Exception:traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_8cc0eef3b32707349c6a7e3f5b2059c8e51ffc75862db16d8a0f0415d473c338->leave($__internal_8cc0eef3b32707349c6a7e3f5b2059c8e51ffc75862db16d8a0f0415d473c338_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  28 => 3,  24 => 2,  22 => 1,);
    }
}
/* {% if exception.trace|length %}*/
/* {% for trace in exception.trace %}*/
/* {% include 'TwigBundle:Exception:trace.txt.twig' with { 'trace': trace } only %}*/
/* */
/* {% endfor %}*/
/* {% endif %}*/
/* */
