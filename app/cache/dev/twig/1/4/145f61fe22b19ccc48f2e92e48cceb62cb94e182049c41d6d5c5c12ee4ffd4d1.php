<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_073dfbb064604022104037c9e55054b59c5de866615d8e5a03a3c4bf85a8bb1e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9349af47316eb742464523bfc77b92938ee2f85f5ca1d20e8a05fced8a118d3e = $this->env->getExtension("native_profiler");
        $__internal_9349af47316eb742464523bfc77b92938ee2f85f5ca1d20e8a05fced8a118d3e->enter($__internal_9349af47316eb742464523bfc77b92938ee2f85f5ca1d20e8a05fced8a118d3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9349af47316eb742464523bfc77b92938ee2f85f5ca1d20e8a05fced8a118d3e->leave($__internal_9349af47316eb742464523bfc77b92938ee2f85f5ca1d20e8a05fced8a118d3e_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a8cdac31ee1cdda67bacc35b4121318763cec3a220a035573ccc58cfd9fdff7d = $this->env->getExtension("native_profiler");
        $__internal_a8cdac31ee1cdda67bacc35b4121318763cec3a220a035573ccc58cfd9fdff7d->enter($__internal_a8cdac31ee1cdda67bacc35b4121318763cec3a220a035573ccc58cfd9fdff7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_a8cdac31ee1cdda67bacc35b4121318763cec3a220a035573ccc58cfd9fdff7d->leave($__internal_a8cdac31ee1cdda67bacc35b4121318763cec3a220a035573ccc58cfd9fdff7d_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_e4f2475a27335137abddeb03f73b51b1958da001fd2fa43a916a063edeb5a2b9 = $this->env->getExtension("native_profiler");
        $__internal_e4f2475a27335137abddeb03f73b51b1958da001fd2fa43a916a063edeb5a2b9->enter($__internal_e4f2475a27335137abddeb03f73b51b1958da001fd2fa43a916a063edeb5a2b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_e4f2475a27335137abddeb03f73b51b1958da001fd2fa43a916a063edeb5a2b9->leave($__internal_e4f2475a27335137abddeb03f73b51b1958da001fd2fa43a916a063edeb5a2b9_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_9a85685decae0750baece6d19e2bca9dcbf608e5aefaef5435933f67ba0ef16f = $this->env->getExtension("native_profiler");
        $__internal_9a85685decae0750baece6d19e2bca9dcbf608e5aefaef5435933f67ba0ef16f->enter($__internal_9a85685decae0750baece6d19e2bca9dcbf608e5aefaef5435933f67ba0ef16f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_9a85685decae0750baece6d19e2bca9dcbf608e5aefaef5435933f67ba0ef16f->leave($__internal_9a85685decae0750baece6d19e2bca9dcbf608e5aefaef5435933f67ba0ef16f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
