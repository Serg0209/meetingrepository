<?php

/* AppBundle::new.html.twig */
class __TwigTemplate_750d200ea556efe093f1262fca13010f4c4bb3a6df112b65d6cd9ca609d284b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle::new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66a570aff9f2b912d26586cffaa0bc663782e33f7b9ef95c850d0279ec9fcb4f = $this->env->getExtension("native_profiler");
        $__internal_66a570aff9f2b912d26586cffaa0bc663782e33f7b9ef95c850d0279ec9fcb4f->enter($__internal_66a570aff9f2b912d26586cffaa0bc663782e33f7b9ef95c850d0279ec9fcb4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_66a570aff9f2b912d26586cffaa0bc663782e33f7b9ef95c850d0279ec9fcb4f->leave($__internal_66a570aff9f2b912d26586cffaa0bc663782e33f7b9ef95c850d0279ec9fcb4f_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_64b8c2e3c9367a39567246510f0b6781be2db4573117d5de3f72b5ed1147a451 = $this->env->getExtension("native_profiler");
        $__internal_64b8c2e3c9367a39567246510f0b6781be2db4573117d5de3f72b5ed1147a451->enter($__internal_64b8c2e3c9367a39567246510f0b6781be2db4573117d5de3f72b5ed1147a451_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "bootstrap_3_layout.html.twig"));
        // line 4
        echo "    ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
";
        
        $__internal_64b8c2e3c9367a39567246510f0b6781be2db4573117d5de3f72b5ed1147a451->leave($__internal_64b8c2e3c9367a39567246510f0b6781be2db4573117d5de3f72b5ed1147a451_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle::new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 4,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block body %}*/
/*     {% form_theme form 'bootstrap_3_layout.html.twig' %}*/
/*     {{ form(form) }}*/
/* {% endblock %}*/
